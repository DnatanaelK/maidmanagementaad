package com.example.danielnk.maidmanagementaad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText et1, et2;
    Button btn1;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new DatabaseHelper(this);
        et1 = (EditText) findViewById(R.id.loginEmail);
        et2 = (EditText) findViewById(R.id.loginPassword);
        btn1 = (Button) findViewById(R.id.loginBTN);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = et1.getText().toString();
                String password = et2.getText().toString();
                Boolean checklogin = db.emailpass(email,password);
                if(checklogin == true){
                    Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
                    Intent h = new Intent(LoginActivity.this, HomePage.class);
                    startActivity(h);
                }else{
                    Toast.makeText(getApplicationContext(), "Login Failed",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
