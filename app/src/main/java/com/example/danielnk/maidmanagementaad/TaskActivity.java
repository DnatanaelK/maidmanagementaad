package com.example.danielnk.maidmanagementaad;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TaskActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    EditText nameET, nationalityET, religionET, marriageET, birthET, employmentET, idET;
    Button addBTN, viewallBTN, updateBTN, deleteBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        myDb = new DatabaseHelper(this);

        nameET = (EditText) findViewById(R.id.nameET);
        nationalityET = (EditText) findViewById(R.id.nationalityET);
        religionET = (EditText) findViewById(R.id.religionET);
        marriageET = (EditText) findViewById(R.id.marriageET);
        birthET = (EditText) findViewById(R.id.birthET);
        employmentET = (EditText) findViewById(R.id.employmentET);
        idET = (EditText) findViewById(R.id.idET);
        addBTN = (Button) findViewById(R.id.addBTN);
        viewallBTN = (Button) findViewById(R.id.viewallBTN);
        updateBTN = (Button) findViewById(R.id.updateBTN);
        deleteBTN = (Button) findViewById(R.id.deleteBTN);
        addData();
        viewall();
        updateData();
        deleteData();
    }
    public void addData(){
        addBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                         boolean isInserted = myDb.insertData(
                                nameET.getText().toString(),
                                nationalityET.getText().toString(),
                                religionET.getText().toString(),
                                marriageET.getText().toString(),
                                birthET.getText().toString(),
                                employmentET.getText().toString()
                                );
                         if (isInserted == true)
                             Toast.makeText(TaskActivity.this, "Data Successfully inserted!", Toast.LENGTH_LONG).show();
                         else
                             Toast.makeText(TaskActivity.this, "Data Failed to insert!", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void viewall(){
        viewallBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor results = myDb.getAllData();
                        if(results.getCount() == 0){
                            showMessage("Error!", "No data found!");
                            return;
                        }
                        StringBuffer sBuffer = new StringBuffer();
                        while (results.moveToNext()){
                            sBuffer.append("Id: "+ results.getString(0)+ "\n");
                            sBuffer.append("Name: "+ results.getString(1)+ "\n");
                            sBuffer.append("Nationality: "+ results.getString(2)+ "\n");
                            sBuffer.append("Religion: "+ results.getString(3)+ "\n");
                            sBuffer.append("Marriage Status: "+ results.getString(4)+ "\n");
                            sBuffer.append("Date of Birth: "+ results.getString(5)+ "\n");
                            sBuffer.append("Employment Status: "+ results.getString(6)+ "\n\n");
                        }

                        showMessage("All Data", sBuffer.toString());

                    }
                }
        );
    }
    public void showMessage (String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
    public void updateData(){
        updateBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isUpdate = myDb.updateData(
                                idET.getText().toString(),
                                nameET.getText().toString(),
                                nationalityET.getText().toString(),
                                religionET.getText().toString(),
                                marriageET.getText().toString(),
                                birthET.getText().toString(),
                                employmentET.getText().toString());
                        if(isUpdate == true) {
                            Toast.makeText(TaskActivity.this, "Data Successfully updated!", Toast.LENGTH_LONG).show();
                        }
                        else
                                Toast.makeText(TaskActivity.this, "Data Failed to update!", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
    public void deleteData(){
        deleteBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer deleted = myDb.deleteData(idET.getText().toString());
                        if(deleted > 0){
                            Toast.makeText(TaskActivity.this, "Data Successfully deleted", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(TaskActivity.this, "Data Failed to delete!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }


}
