package com.example.danielnk.maidmanagementaad;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> ActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity nActivity = null;

    @Before
    public void setUp() throws Exception {
        nActivity = ActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch(){
        View view = nActivity.findViewById(R.id.emailET);
        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        nActivity = null;
    }
}