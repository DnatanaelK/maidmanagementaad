package com.example.danielnk.maidmanagementaad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomePage extends AppCompatActivity {
    DatabaseHelper db;
    Button GetStartedBtn;
    Button LogoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        GetStartedBtn = (Button) findViewById(R.id.GetStartedBtn);
        LogoutBtn = (Button) findViewById(R.id.LogoutBtn);

        GetStartedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTaskActivity();
            }
        });

        LogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToLogin();
            }
        });
    }
    public void backToLogin(){
        Intent u = new Intent(this, LoginActivity.class);
        startActivity(u);
        Toast.makeText(HomePage.this, "Logout Successful", Toast.LENGTH_LONG).show();
    }
    public void openTaskActivity(){
        Intent intent= new Intent(this, TaskActivity.class);
        startActivity(intent);
    }
}
