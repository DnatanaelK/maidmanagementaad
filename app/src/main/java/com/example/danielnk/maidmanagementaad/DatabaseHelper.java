package com.example.danielnk.maidmanagementaad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{

    public static final String DATABSE_NAME = "MaidManagementDB";
    public static final String TABLE_NAME = "maid_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "NATIONALITY";
    public static final String COL_4 = "RELIGION";
    public static final String COL_5 = "MARRIAGE_STATUS";
    public static final String COL_6 = "DATE_OF_BIRTH";
    public static final String COL_7 = "EMPLOYMENT_STATUS";

    public DatabaseHelper(Context context) {
        super(context,DATABSE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table user(email text primary key, password text)");
        db.execSQL("Create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT, NATIONALITY TEXT NOT NULL, RELIGION TEXT NOT NULL, MARRIAGE_STATUS TEXT NOT NULL, DATE_OF_BIRTH DATE NOT NULL, EMPLOYMENT_STATUS NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
        db.execSQL("drop table if exists " +TABLE_NAME);
        onCreate(db);
    }
    // For Registration
    public boolean insertUser(String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("email", email);
        contentValues.put("password", password);
        long ins = db.insert("user", null, contentValues);
        if (ins == -1){
            return false;
        } else{
            return true;
        }
    }
    public Boolean checkemail (String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email=?", new String[]{email});
        if(cursor.getCount()>0) {
            return false;
        } else{
            return true;
        }
    }
    // For Login
    public Boolean emailpass(String email, String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email=? and password=?", new String[]{email,password});
        if(cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }
    //For adding Data
    public Boolean insertData(String NAME,String NATIONALITY,String RELIGION, String MARRIAGE_STATUS, String DATE_OF_BIRTH , String EMPLOYMENT_STATUS  ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, NAME);
        contentValues.put(COL_3, NATIONALITY);
        contentValues.put(COL_4, RELIGION);
        contentValues.put(COL_5, MARRIAGE_STATUS);
        contentValues.put(COL_6, DATE_OF_BIRTH);
        contentValues.put(COL_7, EMPLOYMENT_STATUS);
        long result = db.insert(TABLE_NAME, null,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor results = db.rawQuery("SELECT * from " +TABLE_NAME, null);
        return results;
    }

    public boolean updateData(String ID, String NAME,String NATIONALITY,String RELIGION, String MARRIAGE_STATUS, String DATE_OF_BIRTH , String EMPLOYMENT_STATUS  ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, ID);
        contentValues.put(COL_2, NAME);
        contentValues.put(COL_3, NATIONALITY);
        contentValues.put(COL_4, RELIGION);
        contentValues.put(COL_5, MARRIAGE_STATUS);
        contentValues.put(COL_6, DATE_OF_BIRTH);
        contentValues.put(COL_7, EMPLOYMENT_STATUS);
        int result = db.update(TABLE_NAME,contentValues, "ID = ?",new String[] {ID});
        if (result == 0) {
            return false;
        } else {
            return true;
        }
    }

    public Integer deleteData(String ID){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?",new String[]{ID});
    }
}
