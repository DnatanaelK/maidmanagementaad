package com.example.danielnk.maidmanagementaad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper db;
    EditText et1,et2,et3;
    Button btn1;
    TextView tv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);

        et1 = (EditText)findViewById(R.id.emailET);
        et2 = (EditText)findViewById(R.id.passwordET);
        et3 = (EditText)findViewById(R.id.cpasswordET);
        btn1 = (Button)findViewById(R.id.registerBTN);
        tv1 = (TextView) findViewById(R.id.loginTV);

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1 = et1.getText().toString();
                String s2 = et2.getText().toString();
                String s3 = et3.getText().toString();
                if(s1.equals("") || s2.equals("") || s3.equals("")){
                    Toast.makeText(getApplicationContext(), "Please fill in the fields", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(s2.equals(s3)){
                        Boolean checkemail = db.checkemail(s1);
                        if(checkemail == true){
                            Boolean insertUser = db.insertUser(s1,s2);
                            if(insertUser == true){
                                Toast.makeText(getApplicationContext(),"Registration is Successful", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext()," Email already used", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Password does not match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
